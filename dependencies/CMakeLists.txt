# Thomas Hahn, 2020
#
# Project: cmake-deps
# File: CMakeLists.txt file to handle the dependencies

# module GetDependencies
include(GetDependencies)

# output information
message(STATUS "Preparing dependencies...")

# usedep library
if(BUILD_USEDEP)
    fetch_from_git(
        CONTENT_NAME usedep-lib
        GIT_REPOSITORY https://thoemi01@bitbucket.org/thoemi01/use-dep.git
        GIT_TAG origin/master
        SOURCE_DIRECTORY ${SOURCE_USEDEP})
else()
    # specify UseDep_DIR to help find the package
    message(STATUS "---------------------")
    message(STATUS "Finding UseDep ...")
    find_package(UseDep REQUIRED)
    set_target_properties(usedep::usedep PROPERTIES IMPORTED_GLOBAL TRUE)
    if (TARGET usedep::usedep)
        message(STATUS "UseDep found: UseDep_DIR = ${UseDep_DIR}")
    endif()
    message(STATUS "---------------------")
endif()

# output information
message(STATUS "Dependencies are prepared")
