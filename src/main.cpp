#include <iostream>
#include <dep/dep.h>
#include <use-dep/use-dep.h>

int main() {
    print_dep();
    print_use_dep();
}
